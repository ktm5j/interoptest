#include "libzfs.h"

#include <stdio.h>

int callb_iter(zfs_handle_t *zhp, void *data) {
    zpool_handle_t *zph = zfs_get_pool_handle(zhp);
    const char *zname = zpool_get_name(zph);
    printf("Pool: %s\n", zname);

    return 0;
}

int do_list() {
    libzfs_handle_t *libzfs;
    libzfs = libzfs_init();

    zfs_iter_root(libzfs, callb_iter, 0);
    return 0;
}
